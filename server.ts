import express from 'express';
import cors from 'cors';
// import { configurePassport } from '../utils/token';
import passport from 'passport';
import { configurePassport } from './utils/token';
// import { userRouter } from './controller/userController';
// import { articlesRouter } from './controller/articlesController';
// import { topicsRouter } from './controller/topicsController';
// import { categoryRouter } from './controller/categoryController';

export const server = express();
configurePassport()
server.use(express.json())
server.use(cors());
server.use(passport.initialize())
server.use(express.static('public'))


server.use('/api/user', userRouter)
server.use('/api/article', articlesRouter)
server.use('/api/topic', topicsRouter)
server.use('/api/category', categoryRouter)



