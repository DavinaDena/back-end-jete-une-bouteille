import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { Comment } from "./Comment";



@Entity()

export class Post{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    date: Date;

    @Column()
    title: string;

    @Column()
    text: string;

    @ManyToOne(()=> User, user => user.post)
    user: User[]

    @OneToMany(()=> Comment, comment => comment.post)
    comment: Comment[]

    @ManyToOne(()=> Event , event => event.post)
    event: Event[]
}