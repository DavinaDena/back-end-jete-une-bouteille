import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Post } from "./Post";
import { Comment } from "./Comment";



@Entity()

export class User{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    username: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    nickname: string;

    @Column()
    location: string;

    // @Column()
    // birthdate: string;

    @Column({default: 18})
    age: string;

    @Column({default: 'user'})
    role: string;

    @OneToMany(()=> Post, post => post.user)
    post: Post[];

    @OneToMany(()=> Comment, comment => comment.user)
    comment: Comment[];

    @OneToMany(()=> Event, event => event.user)
    event: Event[];
}