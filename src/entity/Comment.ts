import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Post } from "./Post";
import { User } from "./User";




@Entity()

export class Comment{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    date: Date;

    @Column()
    text: string;

    @ManyToOne(()=> Post, post => post.user)
    post: Post[]

    @ManyToOne(()=> User, user => user.comment)
    user: User[]
}