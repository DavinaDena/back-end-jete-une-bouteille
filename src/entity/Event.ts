import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { Post } from "./Post";




@Entity()

export class Event{

    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name: string;

    @Column()
    description: string;

    @Column()
    date: Date;

    @ManyToOne(()=> User, user => user.event)
    user: User[]

    @OneToMany(()=> Post, post => post.event)
    post: Post[]
}